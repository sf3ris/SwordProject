#include "arrStructs.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

void print(struct ArrayOcc *r){
	assert(r != NULL);
	assert(r->oArr != NULL);
	int i = 0;
	for (i = 0; i < r->len; i++){
			printf("%s : %i \n",r->oArr[i].word,r->oArr[i].number);
			free(r->oArr[i].word);
		}
	printf("\nlen : %d , alloc %d\n",r->len,r->alloc);
}

void printToFile(struct ArrayOcc *r, char *fileName){
	FILE *output;
	output = fopen(fileName,"w+");
	if(!output){
		printf("Error opening file for output : %s\n",fileName);
		exit(0);
	}
	 for (int i = 0; i < r->len; i++){
             fprintf(output, "%s %i\n", r->oArr[i].word, r->oArr[i].number);
             free(r->oArr[i].word);
     }
}

int grow(struct ArrayOcc *r){
	assert(r != NULL);
	assert(r->oArr != NULL);
	struct Occ *newlist = malloc(2*r->alloc*sizeof(*r->oArr));
	memcpy(newlist, r->oArr,sizeof(struct Occ)*r->len);
	if(newlist == NULL){
		return 0; // Failure
	}
	free(r->oArr);
	r->oArr = newlist;
	r->alloc *= 2;
	return 1;
}

int append(struct ArrayOcc *r, char *value){
	assert(r != NULL);
	assert(r->oArr != NULL);
	if(r->len == r->alloc - 1){
			grow(r);
		}
		r->oArr[r->len].word = value;
		r->oArr[r->len].number = 1;
		r->len++;
}

int init(struct ArrayOcc *r, int init_size){
	//r = malloc(sizeof(*r));
	r->len = 0;
	r->alloc = init_size;
	r->oArr = malloc(r->alloc * sizeof(*r->oArr));
	return 1;
}

int addOne(struct ArrayOcc *r, int index){
	assert(r != NULL);
	assert(r->oArr != NULL);
	r->oArr[index].number++;
	return 1;
}