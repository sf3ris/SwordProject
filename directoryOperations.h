#ifndef DIROP
#define DIROP

#include <stdlib.h>

struct dirList{
	char **dArr;
	int len;
	int alloc;
};

void directoryHandler(char *,struct dirList *dlist);
void subDirHandling(char *,struct dirList *dlist);
void subFileHandling(char *filename);
int initDirList(struct dirList *dlist,int initial_size);
int growDirList(struct dirList *dlist);
int appendDir(struct dirList *dlist, char *value);
void printDirList(struct dirList *dlist);
void freeDirList(struct dirList *dlist);



#endif