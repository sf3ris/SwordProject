#include "directoryOperations.h"
#include "threadOperations.h"
#include "fileOperations.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/param.h>
#include <limits.h>
#include <dirent.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>

extern objThread tArr[];
extern int rFlag, eFlag;
extern char *eValue;

void directoryHandler(char *directoryName,struct dirList *dlist){


	char cwd[MAXPATHLEN],*fileName,absPath[MAXPATHLEN];
	getcwd(cwd,MAXPATHLEN);
	DIR * directory;
	struct dirent *sd;

	directory = opendir(directoryName);
	chdir(directoryName);
	if(!directory){
		("Error opening directory : %s\n",directoryName);
	}
	while((sd = readdir(directory)) != NULL){
		fileName = sd->d_name;
        getcwd(absPath, sizeof(absPath));
        strcat(strcat(absPath,"/"),fileName);

		if((sd->d_type == 4) && (strcmp(sd->d_name,".") != 0) && (strcmp(sd->d_name,"..") != 0)){
			if(rFlag == 1 ){
				subDirHandling(absPath,dlist);
			}
		}
		else if(sd->d_type == 8){
			subFileHandling(absPath);
		}

	}
	closedir(directory);
	chdir(cwd);

}

void subDirHandling(char *dir, struct dirList *dlist){
    char *tmpDir = malloc(strlen(dir)+1);
    strncpy(tmpDir,dir,strlen(dir)+1);
	appendDir(dlist, tmpDir);
}

void subFileHandling(char *filename){
	char *tmpFile = malloc(strlen(filename)+1);
    strncpy(tmpFile,filename, strlen(filename)+1);
    char *ext = strrchr(filename,'.');
    int ft;
    if(ext && (strcasecmp(ext,".txt") == 0)){
    	
	    	ft = findFreeThread();
	    	tArr[ft].path = tmpFile;
	    	tArr[ft].status = 1;
	    	printf("%s\n",filename);
	    	pthread_create(&tArr[ft].th,NULL,fileHandler,&tArr[ft]);
	    
    }
    else{
    	printf("Not Valid file : %s\n",filename);	
    	free(tmpFile);
    }

}


int initDirList(struct dirList *dlist, int initial_size){
	dlist->len = 0;
	dlist->alloc = initial_size;
	dlist->dArr = malloc(dlist->alloc * sizeof(char*));
	return 1;
}

int growDirList(struct dirList *dlist){
	assert(dlist != NULL);
	struct dirList *newlist = malloc(2*dlist->alloc*sizeof(*dlist->dArr));
	memcpy(newlist, dlist->dArr,sizeof(struct dirList)*dlist->len);
	if(newlist == NULL){
		return 0; // Failure
	}
	free(dlist->dArr);
	dlist->dArr = newlist;
	dlist->alloc *= 2;
	return 1;
}

int appendDir(struct dirList *dlist, char *value){
	assert(dlist != NULL);
	assert(dlist->dArr != NULL);
	if(dlist->len == dlist->alloc - 1){
			growDirList(dlist);
		}
	dlist->dArr[dlist->len] = value;
	dlist->len++;	
}

void printDirList(struct dirList *dlist){
	assert(dlist != NULL);
	assert(dlist->dArr != NULL);
	int i = 0;
	for (i = 0; i < dlist->len; i++){
			printf("ArrayDirectory[%i] : %s \n",i,dlist->dArr[i]);
			free(dlist->dArr[i]);
		}
	printf("\nDirlen : %d , Diralloc %d\n",dlist->len,dlist->alloc);
}

void freeDirList(struct dirList *dlist){
	assert(dlist != NULL);
	assert(dlist->dArr != NULL);
	int i = 0;
	for (i = 0; i < dlist->len; i++){
		free(dlist->dArr[i]);
	}
}	

