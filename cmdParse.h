#ifndef CMDPARSER
#define CMDPARSER

#include <stdlib.h>

void cmdParser(char **arguments,int nArgs );
void printHelp();
void addToInputArray(char *arg, int index);

#endif