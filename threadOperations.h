#ifndef THREADOP
#define THREADOP

#define NTHREAD 5

#include <stdlib.h>

typedef struct objThread{
	pthread_t th;
	int index;
	int status;
	char *path;
}objThread;

int initThreads();
int findFreeThread();
int joinThreads();


#endif
