#include "threadOperations.h"
#include <pthread.h>
#include <stdio.h>

extern objThread tArr[];

int initTh(){
	int i=0;
	for(i = 0; i < NTHREAD; i++){
		tArr[i].status = 0;
		tArr[i].index = i;
	}

}

int findFreeThread(){
	int i=0;
	while(1){
		for (i = 0; i < NTHREAD; i++){
			if(tArr[i].status == 0) return i;
		}

	}
}

int joinThreads(){
	int i = 0;
	for (i = 0; i < NTHREAD; i++)
	{
		if(tArr[i].status == 1){
			pthread_join(tArr[i].th,NULL);
		}
	}
}

int clearThread(int index){
	free(tArr[index].path);
	tArr[index].status = 0;
}