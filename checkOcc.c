#include "checkOcc.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "arrStructs.h"
#include <stdio.h>

extern struct ArrayOcc *result;

int checkOccurrency(char *strToCheck){
	int i = 0;
	assert(result != NULL);
	char *tmp = malloc(strlen(strToCheck)+1);
	strncpy(tmp,strToCheck,strlen(strToCheck)+1);
	if(result->len == 0) 
		{
			append(result,tmp);
			return -1;
		}
	
	for(i = 0; i < result->len; ++i){
		if(strcasecmp(result->oArr[i].word,tmp) == 0){
			addOne(result,i);
			free(tmp);
			return 0;
		}
	}
	append(result,tmp);
	return -1;
}