#include "arrStructs.h"
#include <stdio.h>
#include <string.h>

#define NTEST 1250

int main(){

	struct ArrayOcc result;
	struct ArrayOcc r2;

	result.len = 0;
	result.alloc = 8;
	result.oArr = malloc(result.alloc * sizeof(*result.oArr));

	int i = 0;

	for (i = 0; i<NTEST; i++){
		if(i == result.alloc){
			r2.oArr = realloc(result.oArr,2*result.alloc * sizeof(*result.oArr));
			result.alloc *= 2;
		}
		result.oArr[i].word = "ciao";
		result.oArr[i].number = i;
		result.len++;
	}


	for (i = 0; i<NTEST; i++){
		printf("%s,%i || ",result.oArr[i].word,result.oArr[i].number);
	}

	printf("\nlen : %d , alloc %d\n",result.len,result.alloc);


	free(result.oArr);

}