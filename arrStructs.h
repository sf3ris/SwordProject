#ifndef sDINARR
#define sDINARR

#include <stdlib.h>
#define NTEST 10

struct Occ{
	char *word;
	int number;
};

struct ArrayOcc{
	int len;
	int alloc;
	struct Occ *oArr;
};

void print(struct ArrayOcc *r);
//void printToFile(struct ArrayOcc *r, char *fileName);
int grow(struct ArrayOcc *r);
int append(struct ArrayOcc *r, char *value);
int init(struct ArrayOcc *r, int init_size);
int addOne(struct ArrayOcc *r, int index);

#endif