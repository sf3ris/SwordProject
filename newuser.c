#include "arrStructs.h"
#include "checkOcc.h"
#include "fileOperations.h"
#include "directoryOperations.h"
#include "threadOperations.h"
#include "cmdParse.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

pthread_mutex_t lock;
struct ArrayOcc *result;
objThread tArr[NTHREAD];
//Declare flags variable for user input
int hFlag = 0, rFlag = 0, fFlag = 0, eFlag = 0, aFlag = 0, mFlag = 0, iFlag = 0, sFlag = 0;
//Declare variables for user input value
char *eValue = NULL, *iValue = NULL;
int mValue;

int main(int argc, char **argv){

	cmdParser(argv,argc);

	//Create Occurency result structure
	struct ArrayOcc *r2;
	//Create directories list structure
	struct dirList *dlist;
	//Allocate memory of 2 structures
	dlist = malloc(sizeof(*dlist));
	result = malloc(sizeof(*result));
	//Initializre result structure
	init(result,8);
	//Initialize threads
	initTh();
	//Initialize directories list structure
	initDirList(dlist,2);

	//TODO USER INPUT EXECUTION
	//directoryHandler("../../Code/",dlist);
	directoryHandler("/home/sf3/Documents/University/Os-Lab/txtFolder/txtDirectory",dlist);

	//TODO RECURSIVE CHECK + EXECUTION

	joinThreads();
	printToFile(result,"Output.txt");

	freeDirList(dlist);
	free(dlist->dArr);
	free(dlist);
	free(result->oArr);
	free(result);

}


