#include "fileOperations.h"
#include "threadOperations.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <pthread.h>
#include "checkOcc.h"
#include "arrStructs.h"

extern pthread_mutex_t lock;
extern struct ArrayOcc *result;

//void fileHandler(char *fileToRead){
void *fileHandler(void *object){
	objThread *oT = object;
	int j = findFreeThread();

	FILE *fstream;

	//char *tmpFile = malloc(strlen(oT->path)+1);
	//strncpy(tmpFile,oT->path,strlen(oT->path)+1);
	//char *tmpFile = strdup(oT->path);


	int c,i=0,max,ret;
	char *tmpW;
	tmpW = calloc(sizeof(char),1);

	//fstream = fopen(tmpFile,"r+");
	fstream = fopen(oT->path,"r+");
	if(!fstream){
		printf("Error opening file stream : %s\n",oT->path);
		//free(tmpFile);
		free(tmpW);
		oT->status = 0;
		return NULL;
	}
	else{
		//printf("File opened correcrly : %s\n",oT->path);
	}
	free(oT->path);

	while(!feof(fstream)){
		c = fgetc(fstream);
		if(c == EOF) break;
		if(isalnum(c) != 0){
			tmpW[i] = c;
			i++;
			if(i > max){
				max = i;
				tmpW = realloc(tmpW,i + 1);
			}
		}
		else if((ispunct(c) != 0) || (isspace(c) != 0)){
			tmpW[i] = '\0';
			if(i>0){
				pthread_mutex_lock(&lock);
				checkOccurrency(tmpW);
				pthread_mutex_unlock(&lock);
			}
			i = 0;
		}
	}
	free(tmpW);
	fclose(fstream);
	//free(tmpFile);
	oT->status = 0;
	return NULL;
}

void retrieveStrings(void *file){

}