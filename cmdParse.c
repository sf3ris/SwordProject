#include "cmdParse.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <limits.h>

extern int hFlag, rFlag, fFlag, eFlag, aFlag, mFlag, iFlag, sFlag;
extern char *eValue, *iValue;
extern int mValue;

char **iArr;
int indexI = 0;

void cmdParser(char **arguments,int nArgs ){
    int c;

    while (( c = getopt(nArgs,arguments,"hrfe:am:i:s")) != -1){
        switch (c) {
            case 'h':
                printHelp();
                exit(1);
                break;
            case 'r' :
                rFlag = 1;
                break;
            case 'f' :
                fFlag = 1;
                break;
            case 'e' :
                eFlag = 1;
                eValue = optarg;
                break;
            case 'a':
                aFlag = 1;
                break;
            case 'm':
                mFlag = 1;
                mValue = atoi(optarg);
                break;
            case 'i':
                iFlag = 1;
                iValue = optarg;
                break;
            case 's':
                sFlag = 1;
                break;
            case '?':
                if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr,
                             "Unknown option character `\\x%x'.\n",
                             optopt);
                exit(1);
        }


        }
    printf("optind = %i\n",optind);
    //iArr = malloc(10 * sizeof( * iArr));

    //char currDir[PATH_MAX];
    //int cd,cdRet;
    //getcwd(currDir, sizeof(currDir));

    for(int index = optind; index < nArgs; index++){
        printf("Input File %s\n",arguments[index]);
        //TODO ADD INPUT INTO ARRAY OF INPUTS THEN CHECK IF FILE OR DIR
    }
/*
    for(int idx = 0; idx < indexI; idx++){
        printf("Array[%i] : %s\n",idx,iArr[indexI]);
    }*/

}

void addToInputArray(char *arg, int index){

}

void printHelp(){
    FILE *helpFile;
    char *helpFileName = "User/help.txt";
    char fileLine[1024];

    helpFile = fopen(helpFileName,"r+");
    if(!helpFile){
        printf("Error reading Help file..");
    }
    else{
        while(fgets(fileLine, sizeof(fileLine),helpFile) != NULL) {
            printf("%s",fileLine);
        }
    }
}